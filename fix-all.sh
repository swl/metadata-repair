#! /bin/bash

node=$(type -P node nodejs)
fragments=$1

if [ ! -d $fragments ]; then
  echo ERROR: ${fragments} is not a directory >&2
  exit 1
fi

rm -rf fixed
mkdir fixed

echo Fixing SP fragments
for md in ${fragments}/*-sp.xml; do
  ${node} fix.js ${md} >fixed/$(basename ${md})
done


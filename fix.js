var parser  = require('xml2json'),
    pd      = require('pretty-data').pd,
    fs      = require('fs'),
    crypto  = require('crypto'),
    hash    = crypto.createHash('sha256'),
    spFieldOrder = [
      'md:ArtifactResolutionService',
      'md:SingleLogoutService',
      'md:ManageNameIDService',
      'md:NameIDFormat',
      'md:AssertionConsumerService',
      'md:AttributeConsumingService',
      'md:ContactPerson',
    ],
    spFields = {},
    kdFieldOrder = [
      'ds:KeyInfo',
      'md:EncryptionMethod',
    ],
    kdFields = {},
    mdxml = fs.readFileSync(process.argv[2], 'utf8').toString(),
    md;


/*
EntityDescriptor
  Signature
  Extensions
  (RoleDescriptor|IDPSSODescriptor|SPSSODescriptor|AuthnAuthorityDescriptor|AttributeAuthorityDescriptor|PDPDescriptor)+
  Organization
    Extensions?
    OrganizationName*
    OrganizationDisplayName?
    OrganizationURL+
  ContactPerson+
  AdditionalMetadataLocation+

SPSSODescriptor
  ArtifactResolutionService
  SingleLogoutService
  ManageNameIDService
  NameIDFormat
  AssertionConsumerService
  AttributeConsumingService
*/

// remove elements from SPSSODescriptor

var namespaces = {
  'md': 'urn:oasis:names:tc:SAML:2.0:metadata',
  'ds': 'http://www.w3.org/2000/09/xmldsig#',
  'alg': 'urn:oasis:names:tc:SAML:metadata:algsupport',
  'init': 'urn:oasis:names:tc:SAML:profiles:SSO:request-init',
  'disco': 'urn:oasis:names:tc:SAML:profiles:SSO:idp-discovery-protocol',
  'enc': 'http://www.w3.org/2001/04/xmlenc#',
};

var elementNs = {
  'Signature': 'ds',
  'KeyInfo': 'ds',
  'DiscoveryResponse': 'disco',
  'KeySize': 'enc',
};

function fix_namespace(el, ns) {
  ns = ns || 'md';
  
  Object.keys(el).forEach(function (childName) {
    var elName    = childName,
        match     = childName.match(/^(\S+):(\S+)$/),
        baseNs    = match ? match[1] : undefined,
        baseName  = match ? match[2] : childName,
        elNs      = baseNs || elementNs[baseName] || ns;

    if (typeof el[childName] !== 'string') {
      // remove default namespace
      Object.keys(namespaces)
      .map(function (ns) {
        return 'xmlns:' + ns;
      })
      .concat(['xmlns'])
      .forEach(function (xmlns) {
        if (el[xmlns]) {
          delete el[xmlns];
        }
      });
      if (!childName.match(/^\d+$/) && childName.substring(0,elNs.length+1) !== elNs + ':') {
        elName = elNs + ':' + baseName;
      }

      el[elName] = fix_namespace(el[childName], elNs);
      if (elName !== childName) {
        delete el[childName];
      }
    }

  });
  return el;
}


/*
md = parser.toJson(xml, {object: true, reversible: true});
console.log('----BEFORE----');
console.dir(md, {depth:null});
console.log('----BEFORE----');
md = fix_namespace(md);
console.log('----AFTER----');
console.dir(md, {depth:null});
console.log('----AFTER----');
*/
md = fix_namespace(parser.toJson(mdxml, {object: true, reversible: true}));

if (md['md:EntitiesDescriptor']) {
  if (md['md:EntitiesDescriptor'] instanceof Array) {
    md = { 'md:EntityDescriptor': md['md:EntitiesDescriptor'][0]['md:EntityDescriptor'] };
  } else {
    md = { 'md:EntityDescriptor': md['md:EntitiesDescriptor']['md:EntityDescriptor'] };
  }
}

var ed = md['md:EntityDescriptor'];
if (ed) {
  var sp      = ed['md:SPSSODescriptor'];

  // remove signature, since we're breaking things
  if (ed['ds:Signature']) {
    delete ed['ds:Signature'];
  }
  
  // replace ID attributes, since people often duplicate them
  // when copying SP metadata, or use incorrect values
  // (the entityID value cannot be used as the ID value)
  hash.update(mdxml);
  ed.ID = '_' + hash.digest('hex');

  Object.keys(namespaces).forEach (function (ns) {
    ed['xmlns:' + ns] = namespaces[ns];
  });
  
  // this is only for SPs, so remove IDPSSODescriptors
  if (ed['md:IDPSSODescriptor']) {
    delete ed['md:IDPSSODescriptor'];
  }

  if (sp) {
    spFieldOrder.forEach(function (f) {
      if (sp[f]) {
        spFields[f] = sp[f];
        delete sp[f];
      }
    });

    // fix incorrectly placed ContactPerson elements
    if (spFields['md:ContactPerson']) {
      ed['md:ContactPerson'] = spFields['md:ContactPerson'];
      delete spFields['md:ContactPerson'];
    }

    // fix incorrectly placed NameIDFormat elements
    var acs = spFields['md:AssertionConsumerService'];
    if (acs && acs['md:NameIDFormat']) {
      spFields['md:NameIDFormat'] = acs['md:NameIDFormat'];
      delete acs['md:NameIDFormat'];
    }

    // insert elements in correct order
    spFieldOrder.forEach(function (f) {
      if (spFields[f]) {
        sp[f] = spFields[f];
      }
    });

    var kd = sp['md:KeyDescriptor'];
    if (kd) {
      if (kd['ds:KeyInfo'] && kd['ds:KeyInfo']['Id']) {
        delete kd['ds:KeyInfo']['Id'];
      }

      kdFieldOrder.forEach(function (f) {
        if (kd[f]) {
          kdFields[f] = kd[f];
          delete kd[f];
        }
      });

      // insert elements in correct order
      kdFieldOrder.forEach(function (f) {
        if (kdFields[f]) {
          kd[f] = kdFields[f];
        }
      });
    }
  }
}

//console.dir(json, {depth:null,colors:false});
console.log(pd.xml(parser.toXml(md)));


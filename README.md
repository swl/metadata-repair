# Fix SAML 2.0 SP Metadata Fragments

Sometimes the SAML 2.0 metadata gets messed up. Invalid IDs are used,
elements are in the wrong order, etc. These tools attempt to fix
metadata.

## setup.sh

Run `setup.sh` to check that the right tools are installed
(`xmlcatalog`, `xmllint`, and `node` or `nodejs`). It also downloads
some XML schema files and creates an XML catalog under the `schema`
directory.

## fix-all.sh

Run `fix-all.sh /path/to/your/fragments/directory` to created fixed
copies of your metadata fragments (those with a `-sp.xml` suffix) under
`fixed`.

## check.sh

Run `check.sh` to check the fixed metadata fragments. Errors are
reported in files under `errors`. `check.sh` will also create a
metadata aggregate (called `Stanford-metadata.xml`) and validate it.
Any fragments that had errors will be included in the aggregate as
comments.


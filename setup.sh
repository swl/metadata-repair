#! /bin/bash

SCHEMATA="\
  http://www.w3.org/2001/xml.xsd \
  http://www.w3.org/TR/2002/REC-xmlenc-core-20021210/xenc-schema.xsd \
  http://www.w3.org/TR/2002/REC-xmldsig-core-20020212/xmldsig-core-schema.xsd \
  http://www.w3.org/TR/xmldsig-core/xmldsig-core-schema.xsd \
  http://docs.oasis-open.org/security/saml/v2.0/saml-schema-metadata-2.0.xsd \
  http://docs.oasis-open.org/security/saml/v2.0/saml-schema-assertion-2.0.xsd \
  http://docs.oasis-open.org/wsfed/federation/v1.2/os/ws-federation.xsd \
  http://docs.oasis-open.org/wsfed/authorization/v1.2/os/ws-authorization.xsd \
  http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd \
"

xmlcatalog=$(type -P xmlcatalog)
if ! type -P xmlcatalog >/dev/null; then
  echo ERROR: failed to find xmlcatalog on PATH >&2
  exit 1
fi

if ! type -P xmllint >/dev/null; then
  echo ERROR: failed to find xmllint on PATH >&2
  exit 1
fi

if ! type -P node nodejs >/dev/null; then
  echo ERROR: failed to find node or nodejs on PATH >&2
  exit 1
fi

if ! type -P npm >/dev/null; then
  echo ERROR: failed to find npm on PATH >&2
  exit 1
fi

if ! type -P curl >/dev/null; then
  echo ERROR: failed to find curl on PATH >&2
  exit 1
fi

[ -d node_modules/xml2json ] || npm install xml2json
[ -d node_modules/pretty-data ] || npm install pretty-data
  
rm -rf schema
mkdir -p schema

xmlcatalog --create > schema/catalog

for xsd in ${SCHEMATA}; do
  filename=${xsd/*\//}
  echo ${filename}
  curl -s -o schema/${filename} ${xsd}
  xmlcatalog --noout --add uri ${xsd} file://${PWD}/schema/${filename} schema/catalog
done

cat <<NEXTSTEPS
Now run:

  ./fix-all.sh path-to-the-SP-fragments
  ./check.sh

then check the errors directory
NEXTSTEPS

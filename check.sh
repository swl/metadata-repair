#! /bin/bash

rm -rf errors
mkdir errors

AGGREGATE=Stanford-metadata.xml
MD_SCHEMA=schema/saml-schema-metadata-2.0.xsd
WSFED_SCHEMA=schema/ws-federation.xsd
SCHEMA="--schema ${MD_SCHEMA} --schema ${WSFED_SCHEMA}"

export XML_CATALOG_FILES=${PWD}/schema/catalog

if [ ! -f ${XML_CATALOG_FILES} ]; then
  echo ${XML_CATALOG_FILES}
  echo No XML catalog - please run ./setup.sh first >&2
  exit 1
fi

if [ ! -d fixed ] || ! ls fixed/*.xml >/dev/null 2>&1; then
  echo No fixed files - please run ./fix-all.sh first >&2
  exit 1
fi


echo Checking fragments
for md in fixed/*.xml; do
  err=errors/$(basename ${md} .xml).txt
  xmllint --noout --nowarning ${SCHEMA} ${md} 2>${err}
  grep -q "^${md} validates\$" ${err} && rm ${err}
done

if ls errors/*.txt >/dev/null 2>&1; then
  echo ERROR: some metadata fragments are invalid: >&2
  ls errors/*.txt | while read err; do
    echo "  $(basename $err .txt).xml" >&2
  done
fi

TMPMD=/tmp/metadata-$$.xml

rm -f ${AGGREGATE}
cat >${AGGREGATE} <<PREFIX
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<EntitiesDescriptor
  xmlns="urn:oasis:names:tc:SAML:2.0:metadata"
  xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
  xmlns:shibmd="urn:mace:shibboleth:metadata:1.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  Name="urn:mace:stanford.edu:shibboleth:stanford"
  validUntil="2020-01-01T00:00:00Z">

PREFIX

for md in fixed/*.xml; do
  prefix=
  suffix=
  if [ -f errors/$(basename ${md} .xml).txt ]; then
    prefix='<!-- INVALID'
    suffix='     INVALID -->'
    echo Commenting out invalid metadata for $(basename ${md}) >&2
  fi
  echo $prefix >>${AGGREGATE}
  cat ${md}    >>${AGGREGATE}
  echo $suffix >>${AGGREGATE}
done

echo >>${AGGREGATE}
echo '</EntitiesDescriptor>' >> ${AGGREGATE}

echo Checking aggregate ${AGGREGATE}
xmllint --noout --nowarning ${SCHEMA} ${AGGREGATE}

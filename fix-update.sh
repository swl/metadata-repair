#! /bin/bash

for err in errors/*.txt; do
  md=~/src/puppet/idg/modules/shibboleth_meta/files/fragments/stanford2/$(basename ${err} .txt).xml
  node fix.js ${md} >sm/$(basename ${md})
done

